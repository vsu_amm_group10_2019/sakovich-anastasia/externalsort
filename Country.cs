﻿namespace ExternalSort
{
    public record Country(string Name, string Continent, string Capital, double Area, int Number, string PoliticalSystem)
    {
        public override string ToString()
            => string.Join(" ", Name, Continent, Capital, Area, Number, PoliticalSystem);
    }
}
