﻿using System.Collections.Generic;

namespace ExternalSort
{
    public class CountryComparer : IComparer<Country>
    {
        public int Compare(Country x, Country y)
            => x.Number.CompareTo(y.Number);
    }

}
