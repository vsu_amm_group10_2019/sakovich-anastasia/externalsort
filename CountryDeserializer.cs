﻿namespace ExternalSort
{
    public class CountryDeserializer : IElementDeserializer<Country>
    {
        public Country Deserialize(string raw)
        {
            string[] temp = raw.Split(' ');
            return new Country(temp[0], temp[1], temp[2], double.Parse(temp[3]), int.Parse(temp[4]), temp[5]);
        }
    }

}
