﻿namespace ExternalSort
{
    public interface IElementDeserializer<T>
    {
        T Deserialize(string raw);
    }
}
