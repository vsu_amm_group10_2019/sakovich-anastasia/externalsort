﻿
namespace ExternalSort
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.oonTable = new System.Windows.Forms.DataGridView();
            this.CountryName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CountryContinent = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CountryCapital = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CountryArea = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CountryPoliticSystem = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.openDialog = new System.Windows.Forms.OpenFileDialog();
            ((System.ComponentModel.ISupportInitialize)(this.oonTable)).BeginInit();
            this.SuspendLayout();
            // 
            // oonTable
            // 
            this.oonTable.AllowUserToAddRows = false;
            this.oonTable.AllowUserToDeleteRows = false;
            this.oonTable.AllowUserToResizeColumns = false;
            this.oonTable.AllowUserToResizeRows = false;
            this.oonTable.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.oonTable.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.oonTable.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.CountryName,
            this.CountryContinent,
            this.CountryCapital,
            this.CountryArea,
            this.dataGridViewTextBoxColumn1,
            this.CountryPoliticSystem});
            this.oonTable.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.oonTable.Location = new System.Drawing.Point(12, 12);
            this.oonTable.Name = "oonTable";
            this.oonTable.RowTemplate.Height = 25;
            this.oonTable.Size = new System.Drawing.Size(643, 270);
            this.oonTable.TabIndex = 0;
            // 
            // CountryName
            // 
            this.CountryName.HeaderText = "Название";
            this.CountryName.Name = "CountryName";
            // 
            // CountryContinent
            // 
            this.CountryContinent.HeaderText = "Континент";
            this.CountryContinent.Name = "CountryContinent";
            // 
            // CountryCapital
            // 
            this.CountryCapital.HeaderText = "Столица";
            this.CountryCapital.Name = "CountryCapital";
            // 
            // CountryArea
            // 
            this.CountryArea.HeaderText = "Площадь";
            this.CountryArea.Name = "CountryArea";
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.HeaderText = "Численность";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            // 
            // CountryPoliticSystem
            // 
            this.CountryPoliticSystem.HeaderText = "Политическая система";
            this.CountryPoliticSystem.Name = "CountryPoliticSystem";
            // 
            // openDialog
            // 
            this.openDialog.Filter = "Все файлы (*.*)|**.*|Текстовые файлы (*.txt)|*.txt";
            this.openDialog.RestoreDirectory = true;
            this.openDialog.Title = "Выберите файл";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(667, 294);
            this.Controls.Add(this.oonTable);
            this.Name = "MainForm";
            this.Text = "External Sort";
            this.Load += new System.EventHandler(this.FormLoad);
            ((System.ComponentModel.ISupportInitialize)(this.oonTable)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView oonTable;
        private System.Windows.Forms.DataGridViewTextBoxColumn CountryName;
        private System.Windows.Forms.DataGridViewTextBoxColumn CountryContinent;
        private System.Windows.Forms.DataGridViewTextBoxColumn CountryCapital;
        private System.Windows.Forms.DataGridViewTextBoxColumn CountryArea;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn CountryPoliticSystem;
        private System.Windows.Forms.OpenFileDialog openDialog;
    }
}