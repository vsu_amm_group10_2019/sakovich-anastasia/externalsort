﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ExternalSort
{
    public partial class MainForm : Form
    {
        private readonly IElementDeserializer<Country> _elementDeserializer;
        private readonly IComparer<Country> _comparer;

        public MainForm()
        {
            InitializeComponent();
            _elementDeserializer = new CountryDeserializer();
            _comparer = new CountryComparer();
        }

        private void FormLoad(object sender, EventArgs e)
        {
            try
            {
                if (openDialog.ShowDialog() == DialogResult.OK)
                {
                    var sorter = new TwoWayFirstPhasedSimpleSort<Country>(_elementDeserializer, _comparer);
                    sorter.SortFile(openDialog.FileName);

                    using var sr = new StreamReader(File.Open(openDialog.FileName, FileMode.Open, FileAccess.Read, FileShare.Read));
                    string line;
                    while ((line = sr.ReadLine()) is not null)
                    {
                        Country country = _elementDeserializer.Deserialize(line);
                        oonTable.Rows.Add(country.Name, country.Continent, country.Capital, country.Area, country.Number, country.PoliticalSystem);
                    }
                }
                else
                    Close();
            }
            catch
            {
                Close();
            }
        }
    }
}
