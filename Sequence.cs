﻿using System;
using System.IO;

namespace ExternalSort
{
    public sealed class Sequence<T> : IDisposable
    {
        private StreamReader _reader;
        private StreamWriter _writer;
        private readonly FileInfo _fileInfo;
        private readonly IElementDeserializer<T> _deserializer;
        private bool _needDelete;

        public T Element { get; private set; }

        public bool ReadyToCopy { get; private set; }

        private int Remaning { get; set; }

        public Sequence(string filePath, IElementDeserializer<T> deserializer, bool needDelete = true)
        {
            _fileInfo = new FileInfo(filePath);
            _needDelete = needDelete;
            _deserializer = deserializer;
        }

        public void NewRun(int length)
            => Remaning = length;

        public void StartRead()
            => _reader = new StreamReader(File.Open(_fileInfo.FullName, FileMode.Open, FileAccess.Read, FileShare.Read));
        
        public void StopRead()
            => _reader.Close();

        public void StartWrite()
            => _writer = new StreamWriter(File.Open(_fileInfo.FullName, FileMode.Create, FileAccess.Write, FileShare.Read));
        
        public void StopWrite()
            => _writer.Close();

        public void WriteElem(T element)
            => _writer.WriteLine(element);

        public bool IsEndOfFile()
            => _reader.EndOfStream;

        public bool IsEndOfSeries()
            => IsEndOfFile() || Remaning == 0;

        public void Dispose()
        {
            if (_fileInfo.Exists && _needDelete)
                _fileInfo.Delete();
            _reader?.Dispose();
            _writer?.Dispose();
        }

        public void ReadElem()
        {
            if (!IsEndOfSeries())
            {
                Element = _deserializer.Deserialize(_reader.ReadLine());
                ReadyToCopy = true;
            }
        }

        public void CopyElementTo(Sequence<T> sequence)
        {
            if (ReadyToCopy)
            {
                sequence.WriteElem(Element);
                Remaning--;
                ReadyToCopy = false;
            }
        }

        public void CopyAllTo(Sequence<T> sequence)
        {
            ReadElem();
            do
            {
                CopyElementTo(sequence);
                ReadElem();
            } while (ReadyToCopy);
        }
    }
}
