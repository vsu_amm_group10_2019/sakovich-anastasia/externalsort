﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ExternalSort
{
    public class TwoWayFirstPhasedSimpleSort<T>
    {
        // Число временных файлов с одной стороны
        private const int OneSideFilesCount = 2;

        // Входная сторона сортировки
        private Sequence<T>[] _inputSide;

        // Выходная сторона сортировки
        private Sequence<T>[] _outputSide;

        // Длина текущей серии
        private int _currentSeriesLength;

        // Последняя серия, в которую записывались элементы
        private Sequence<T> _lastSeries;

        // Условие сортировки
        private readonly IComparer<T> _comparer;

        // Правило чтения элементов
        private readonly IElementDeserializer<T> _deserializer;

        public TwoWayFirstPhasedSimpleSort(IElementDeserializer<T> deserializer, IComparer<T> comparer = null)
        {
            _comparer = comparer ?? Comparer<T>.Default;
            _currentSeriesLength = 1;
            _deserializer = deserializer;
        }

        public TwoWayFirstPhasedSimpleSort(IElementDeserializer<T> deserializer, Comparison<T> comparison)
            : this(deserializer, Comparer<T>.Create(comparison))
        {

        }

        public void SortFile(string fileName)
        {
            using var sequence = new Sequence<T>(fileName, _deserializer, false);
            _inputSide = InitSequences(OneSideFilesCount);
            _outputSide = InitSequences(OneSideFilesCount);

            FirstMerge(sequence, _inputSide);
            while (Merge() > 1)
            {
                _currentSeriesLength *= OneSideFilesCount;
                Swap(ref _inputSide, ref _outputSide);
            }

            sequence.StartWrite();
            _lastSeries.StartRead();
            _lastSeries.NewRun(_currentSeriesLength * OneSideFilesCount);
            _lastSeries.CopyAllTo(sequence);
            _lastSeries.StopRead();
            sequence.StopWrite();

            foreach (Sequence<T> tmp in _inputSide)
                tmp.Dispose();
            _inputSide = null;

            foreach (Sequence<T> tmp in _outputSide)
                tmp.Dispose();
            _outputSide = null;
        }

        private void FirstMerge(Sequence<T> sequence, Sequence<T>[] sequences)
        {
            sequence.StartRead();

            foreach (Sequence<T> tmp in sequences)
                tmp.StartWrite();

            sequence.NewRun(_currentSeriesLength);

            int destination = 0;
            int count = 0;

            while (!sequence.IsEndOfFile())
            {
                destination %= sequences.Length;
                sequence.ReadElem();
                sequence.CopyElementTo(sequences[destination]);
                destination++;
                count++;
                if (count == _currentSeriesLength)
                {
                    sequence.NewRun(_currentSeriesLength);
                    count = 0;
                }
            }

            sequence.StopRead();
            foreach (Sequence<T> tmp in sequences)
                tmp.StopWrite();
        }

        private int Merge()
        {
            int countOfIterations = 0;

            foreach (Sequence<T> source in _inputSide)
            {
                source.StartRead();
                source.NewRun(_currentSeriesLength);
                source.ReadElem();
            }
            foreach (Sequence<T> destination in _outputSide)
                destination.StartWrite();

            int dest = 0;
            int count = 0;
            while (FirstReady(_inputSide) > -1)
            {
                while (FirstReady(_inputSide) > -1)
                {
                    dest %= _outputSide.Length;
                    int i = GetIndexOfBest(_inputSide);
                    _inputSide[i].CopyElementTo(_outputSide[dest]);
                    _inputSide[i].ReadElem();
                    count++;
                    _lastSeries = _outputSide[dest];
                    if (count % (_inputSide.Length * _currentSeriesLength) == 0)
                        dest++;
                }

                countOfIterations++;
                foreach (Sequence<T> tmp in _inputSide)
                {
                    tmp.NewRun(_currentSeriesLength);
                    tmp.ReadElem();
                }
            }

            foreach (Sequence<T> source in _inputSide)
                source.StopRead();
            foreach (Sequence<T> destination in _outputSide)
                destination.StopWrite();

            return countOfIterations;
        }

        private static int FirstReady(Sequence<T>[] sequences)
        {
            int first = 0;
            while (first < sequences.Length && !sequences[first].ReadyToCopy)
                first++;
            return first != sequences.Length ? first : -1;
        }

        private int GetIndexOfBest(Sequence<T>[] sequences)
        {
            int bestIndex = FirstReady(sequences);
            if (bestIndex == -1)
                throw new Exception("Серия пуста.");
            for (int i = bestIndex + 1; i < sequences.Length; i++)
                if (sequences[i].ReadyToCopy && _comparer.Compare(sequences[i].Element, sequences[bestIndex].Element) < 0)
                    bestIndex = i;
            return bestIndex;
        }

        private Sequence<T>[] InitSequences(int count)
            => Enumerable.Range(1, count)
                .Select(x => new Sequence<T>($"{{{ Guid.NewGuid() }}}.bin", _deserializer))
                .ToArray();

        private static void Swap<U>(ref U first, ref U second)
        {
            U temp;
            temp = first;
            first = second;
            second = temp;
        }
    }
}
